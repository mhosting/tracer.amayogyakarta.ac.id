# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :concentration do
    title "MyString"
    value "MyString"
  end
end
