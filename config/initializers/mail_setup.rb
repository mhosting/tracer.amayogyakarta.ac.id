ActionMailer::Base.smtp_settings = {
  :address        => 'smtp.sendgrid.net',
  :port           => 587,
  :authentication => :plain,
  :user_name      => ENV['SENDGRID_USERNAME'],
  :password       => ENV['SENDGRID_PASSWORD'],
  :domain         => 'heroku.com',
  :enable_starttls_auto => true
}
#ActionMailer::Base.smtp_settings = {
#  :port           => '587',
#  :address        => 'smtp.sendgrid.net',
#  :user_name      => 'app35680462@heroku.com',
#  :password       => 'hfvlw8at3924',
#  :domain         => 'pmb.amaypk.ac.id',
#  :authentication => :plain,
#}
ActionMailer::Base.delivery_method = :smtp