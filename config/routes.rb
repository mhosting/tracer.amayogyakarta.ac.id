TracerStudy::Application.routes.draw do
  

  resource :passwords  
  #  get "passwords/edit"
  #  get "passwords/update"
  get "dashboards/index"

  resource :profiles
  resources :profiles
  resources :dashboards
  authenticated :user do
    root :to => 'dashboards#index'
  end
  root :to => "home#index"
  devise_for :users, :controllers => {:registrations => "registrations"}
  #  as :member do
  #    get 'password/edit' => 'devise/registrations#edit', :as => 'member_edit_password'   
  #    put 'password' => 'devise/registrations#update', :as => 'member_user_password'            
  #  end
  resources :users do
    collection do
      get :reset_password
      get :verifikasi
      get :proses
    end
  end
  get "users/:id/reset_password" => "users#reset_password", :as => 'user_reset_password'
  #get "users/verifikasi" => "users#verifikasi", :as => 'user_verifikasi_alumni'
  post "users/proses" => "users#proses", :as => 'user_proses'
  get "admin/users/:id/reset_password" => "admin/users#reset_password", :as => 'admin_user_reset_password'
  get "admin/users/:id/aproval" => "admin/users#aproval", :as => 'admin_user_aproval'
  get "admin/users/:id/unaproval" => "admin/users#unaproval", :as => 'admin_user_unaproval'
  match 'profiles/edit' => 'profiles#edit', :via => :get
  
  namespace :admin do
    resources :dashboards 
    resources :profiles
    resources :cards
    resources :books do
      collection do
        get :download
      end
    end
    resources :users do
      collection do
        get :reset_password
        get :aproval
        get :all_approval
        get :unaproval
      end
    end
  end
  get "admin/books/download/:id" => "admin/books#download", :as => 'admin_book_download_xls'
end

