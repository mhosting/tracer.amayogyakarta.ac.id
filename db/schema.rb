# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20160617031725) do

  create_table "concentrations", :force => true do |t|
    t.string   "title"
    t.string   "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "profiles", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.string   "nim"
    t.string   "nia"
    t.string   "nama"
    t.string   "tempat_lahir"
    t.string   "tanggal_lahir"
    t.text     "alamat_rumah"
    t.string   "nomor_HP"
    t.string   "nomor_telp"
    t.string   "email"
    t.string   "blog"
    t.string   "web"
    t.string   "facebook"
    t.string   "pinbb"
    t.string   "no_wa"
    t.string   "prodi"
    t.string   "konsentrasi"
    t.date     "tahun_masuk"
    t.string   "tempat_pkl"
    t.text     "judul_ta"
    t.string   "nama_instansi_first"
    t.string   "jabatan_first"
    t.string   "tanggal_masuk_kerja_first"
    t.string   "nama_instansi_last"
    t.string   "jabatan_last"
    t.string   "tanggal_masuk_kerja_last"
    t.string   "tgl_lulus"
    t.integer  "concentration_id"
    t.boolean  "approved",                  :default => false, :null => false
    t.string   "ipk"
    t.string   "ortu"
    t.string   "keahlian"
    t.string   "tahun_wisuda"
    t.string   "ipka"
    t.string   "sks_ditempuh"
    t.string   "jmh_bobot_x_kredit"
    t.string   "nilai_lta"
    t.string   "email_avatar"
  end

  add_index "profiles", ["approved"], :name => "index_profiles_on_approved"
  add_index "profiles", ["concentration_id"], :name => "index_profiles_on_concentration_id"
  add_index "profiles", ["user_id"], :name => "index_profiles_on_user_id"

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "name"
    t.string   "username"
    t.boolean  "approved",               :default => false, :null => false
    t.string   "nomor_urut"
  end

  add_index "users", ["approved"], :name => "index_users_on_approved"
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["username"], :name => "index_users_on_username", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

end
