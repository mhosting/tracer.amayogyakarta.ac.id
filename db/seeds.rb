# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# Environment variables (ENV['...']) can be set in the file config/application.yml.
# See http://railsapps.github.io/rails-environment-variables.html
puts 'ROLES'
YAML.load(ENV['ROLES']).each do |role|
  Role.find_or_create_by_name({ :name => role }, :without_protection => true)
  puts 'role: ' << role
end
puts 'DEFAULT USERS'
user = User.find_or_create_by_email :username => ENV['ADMIN_NAME'].dup, :email => ENV['ADMIN_EMAIL'].dup, :password => ENV['ADMIN_PASSWORD'].dup, :password_confirmation => ENV['ADMIN_PASSWORD'].dup, :approved => ENV['ADMIN_STATUS'].dup
puts 'user: ' << user.username
user.add_role :admin
#
Concentration.find_or_create_by_title({ :title => 'Perkantoran dan Sistem Informasi', :value => '01' }, :without_protection => true)
Concentration.find_or_create_by_title({ :title => 'Administrasi Kesehatan', :value => '02' }, :without_protection => true)
Concentration.find_or_create_by_title({ :title => 'Administrasi dan Komputer', :value => '03' }, :without_protection => true)
Concentration.find_or_create_by_title({ :title => 'Administrasi dan SDM', :value => '04' }, :without_protection => true)
Concentration.find_or_create_by_title({ :title => 'Manajemen Administrasi Obat dan Farmasi', :value => '33' }, :without_protection => true)
Concentration.find_or_create_by_title({ :title => 'Manajemen Administrasi Rumah Sakit', :value => '03' }, :without_protection => true)
Concentration.find_or_create_by_title({ :title => 'Manajemen Administrasi Transportasi Udara', :value => '13' }, :without_protection => true)
