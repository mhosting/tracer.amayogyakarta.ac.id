class AddEmailAvatarOnProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :email_avatar, :string
  end
end
