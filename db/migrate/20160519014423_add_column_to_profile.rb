class AddColumnToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :ipka, :string
    add_column :profiles, :sks_ditempuh, :string
    add_column :profiles, :jmh_bobot_x_kredit, :string
    add_column :profiles, :nilai_lta, :string
  end
end
