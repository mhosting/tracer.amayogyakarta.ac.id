class AddIpkToProfile < ActiveRecord::Migration
  def self.up
    add_column :profiles, :ipk, :string
  end

  def self.down
    remove_column :profiles, :ipk
  end
end
