class AddTglLulusToProfile < ActiveRecord::Migration
  def change
    remove_column :profiles, :tahun_lulus
    add_column :profiles, :tgl_lulus, :string
  end
end
