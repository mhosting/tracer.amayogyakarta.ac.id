class AddOrtuToProfile < ActiveRecord::Migration
  def self.up
    add_column :profiles, :ortu, :string
    add_column :users, :nomor_urut, :string
  end

  def self.down
    remove_column :profiles, :ortu
    remove_column :users, :nomor_urut
  end
end
