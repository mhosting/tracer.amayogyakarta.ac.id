class AddTahunWisudaToProfile < ActiveRecord::Migration
  def self.up
    add_column :profiles, :tahun_wisuda, :string
  end

  def self.down
    remove_column :profiles, :tahun_wisuda
  end
end
