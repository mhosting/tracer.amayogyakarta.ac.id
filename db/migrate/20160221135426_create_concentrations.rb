class CreateConcentrations < ActiveRecord::Migration
  def change
    add_column :profiles, :concentration_id, :integer
    add_index :profiles, :concentration_id
    create_table :concentrations do |t|
      t.string :title
      t.string :value

      t.timestamps
    end
  end
end
