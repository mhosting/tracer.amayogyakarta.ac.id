class AddAllProfileToProfile < ActiveRecord::Migration
  def change
    remove_column :profiles, :first_name
    remove_column :profiles, :last_name
    add_column :profiles, :nim, :string
    add_column :profiles, :nia, :string
    add_column :profiles, :nama, :string
    add_column :profiles, :tempat_lahir, :string
    add_column :profiles, :tanggal_lahir, :date
    add_column :profiles, :alamat_rumah, :text
    add_column :profiles, :nomor_HP, :string
    add_column :profiles, :nomor_telp, :string
    add_column :profiles, :email, :string
    add_column :profiles, :blog, :string
    add_column :profiles, :web, :string
    add_column :profiles, :facebook, :string
    add_column :profiles, :pinbb, :string
    add_column :profiles, :no_wa, :string
    add_column :profiles, :prodi, :string
    add_column :profiles, :konsentrasi, :string
    add_column :profiles, :tahun_masuk, :date
    add_column :profiles, :tahun_lulus, :date
    add_column :profiles, :tempat_pkl, :string
    add_column :profiles, :judul_ta, :text
    add_column :profiles, :nama_instansi_first, :string
    add_column :profiles, :jabatan_first, :string
    add_column :profiles, :tanggal_masuk_kerja_first, :date
    add_column :profiles, :nama_instansi_last, :string
    add_column :profiles, :jabatan_last, :string
    add_column :profiles, :tanggal_masuk_kerja_last, :date
  end
end
