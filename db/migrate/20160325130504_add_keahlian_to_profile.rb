class AddKeahlianToProfile < ActiveRecord::Migration
  def self.up
    add_column :profiles, :keahlian, :string
  end

  def self.down
    remove_column :profiles, :keahlian
  end
end
