class ChangeFormatTglJobToProfile < ActiveRecord::Migration
  def up
    change_column :profiles, :tanggal_masuk_kerja_first, :string
    change_column :profiles, :tanggal_masuk_kerja_last, :string
  end

  def down
    change_column :profiles, :tanggal_masuk_kerja_first, :date
    change_column :profiles, :tanggal_masuk_kerja_last, :date
  end
end
