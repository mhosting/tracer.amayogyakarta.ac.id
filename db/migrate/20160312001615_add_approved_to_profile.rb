class AddApprovedToProfile < ActiveRecord::Migration
  def self.up
    add_column :profiles, :approved, :boolean, :default => false, :null => false
    add_index  :profiles, :approved
  end

  def self.down
    remove_index  :profiles, :approved
    remove_column :profiles, :approved
  end
end
