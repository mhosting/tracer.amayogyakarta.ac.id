class Profile < ActiveRecord::Base
  belongs_to :user
  belongs_to :concentration
  attr_accessible :nama, :user_id, :nim, :nia, :ipk, :email, :email_avatar, :tahun_masuk, :tgl_lulus, :concentration_id, :tempat_lahir, :tanggal_lahir,
                  :keahlian, :alamat_rumah, :ortu, :nomor_HP, :no_wa, :nomor_telp, :pinbb, :facebook, :web, :blog, :tempat_pkl, :judul_ta, :nama_instansi_first,
  :jabatan_first, :tanggal_masuk_kerja_first, :nama_instansi_last, :jabatan_last, :tanggal_masuk_kerja_last, :sks_ditempuh, :jmh_bobot_x_kredit, :nilai_lta, :ipka

  attr_accessor :ipka_view, :ipk_view
  validates_presence_of :nama, message: 'Tidak Boleh Kosong.'
  validates_presence_of :tahun_masuk, message: 'Tidak Boleh Kosong.'
  #validates_presence_of :jmh_bobot_x_kredit
  validates_presence_of :tgl_lulus, message: 'Tidak Boleh Kosong.'
  validates_format_of :tgl_lulus, :with => /[0-3]{1}[0-9]{1}\/[0-1]{1}[0-9]{1}\/[1-2]{1}[0-9]{3}/, :message => "Format : DD/MM/YYYY"
  validates_presence_of :tanggal_lahir, message: 'Tidak Boleh Kosong.'
  validates_format_of :tanggal_lahir, :with => /[0-3]{1}[0-9]{1}\/[0-1]{1}[0-9]{1}\/[1-2]{1}[0-9]{3}/, :message => "Format : DD/MM/YYYY"
#  validates_presence_of :ipk
#  validates_format_of :ipk, :with => /^\d+\.*\d{0,2}$/
  
  def build_profile

  end
end
