class Concentration < ActiveRecord::Base
  attr_accessible :title, :value
  has_one :profile
end
