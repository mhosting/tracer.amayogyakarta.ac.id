class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]

  # Setup accessible (or protected) attributes for your model
  attr_accessible :role_ids, :as => :admin
  attr_accessible :login, :username, :name, :email, :password, :password_confirmation, :approved, :remember_me, :nomor_urut, :profile_attributes
  has_one :profile, :dependent => :destroy
  accepts_nested_attributes_for :profile, :reject_if => proc { |attributes| attributes['nama'].blank? }
  validates_associated :profile
  after_create :default_role
  attr_accessor :login
  validates_presence_of :username,
    :presence => true,
    :uniqueness => {
    :case_sensitive => false
  }, :message => 'Tidak Boleh Kosong.'
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      if conditions[:username].nil?
        where(conditions).first
      else
        where(username: conditions[:username]).first
      end
    end
  end
  
  
  def active_for_authentication? 
    super && approved? 
  end 

  def inactive_message 
    if !approved? 
      :not_approved 
    else 
      super # Use whatever other message 
    end 
  end

  private
  
  def default_role
    add_role(:user)
  end
  
end
