class PasswordsController < ApplicationController
  before_filter :authenticate_user!
  
  def edit
    @user = !params[:id].nil? ? User.find(params[:id]) : current_user
  end

  def update
     @user = User.find(current_user.id)
     @user.password = params[:user][:password]
     @user.password_confirmation = params[:user][:password_confirmation]
    if params[:user][:password] == params[:user][:password_confirmation]
      @user.save
      # Sign in the user by passing validation in case their password changed
      sign_in @user, :bypass => true
      redirect_to root_path, notice: 'Password Berhasil di Ganti'
    else
      redirect_to edit_passwords_path, notice: 'Password Tidak Sama'
    end
  end
end
