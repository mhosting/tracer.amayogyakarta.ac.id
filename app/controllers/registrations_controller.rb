class RegistrationsController < Devise::RegistrationsController
  def create
    super
    if resource.save
      resource.profile.user_id = resource.id
      resource.profile.nim = resource.username
      resource.profile.email = resource.email
      resource.profile.email_avatar = resource.email
      resource.profile.save
    end
  end

  def new
    #    super
    build_resource({})
    resource.build_profile 
    respond_with self.resource
  end
  
  def update
    super
  end

end
