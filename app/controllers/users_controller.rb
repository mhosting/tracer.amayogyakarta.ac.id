class UsersController < ApplicationController
  #before_filter :authenticate_user!

  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.includes(:roles).where("roles.name = 'user'")
  end
  
  def new
    authorize! :new, @user, :message => 'Not authorized as an administrator.'
    @user = User.new
  end

  def show
    if  current_user.profile.blank? == true 
      redirect_to new_profiles_url
    else
      @user = User.find(params[:id])
    end
  end
  
  def update
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user], :as => :admin)
      redirect_to users_path, :notice => "User updated."
    else
      redirect_to users_path, :alert => "Unable to update user."
    end
  end
  
  def create
    @user = User.new(params[:user])
    if @user.save
      flash[:notice] = "Successfully created User." 
      redirect_to users_path
    else
      redirect_to users_path
    end
  end
    
  def destroy
    authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => "User deleted."
    else
      redirect_to users_path, :notice => "Can't delete yourself."
    end
  end
  
  def reset_password
    user = User.find(params[:id])
    user.password = '12345678'
    user.password_confirmation = '12345678'
    user.save
    #@user.update_attributes(:password => '12345678', :password_confirmation => '123456789')
    redirect_to users_path, :notice => "Password Reset Successful."
  end
  
  def aproval
    user = User.find(params[:id])
    user.approved = true
    user.save
    #@user.update_attributes(:password => '12345678', :password_confirmation => '123456789')
    redirect_to users_path, :notice => "Password Reset Successful."
  end
  
  def verifikasi
    
  end
  
  def proses
    size = Profile.where(["nim = ?", params[:nim]]).where(["nama = ?", params[:nama]]).where(["approved = ?", true]).size
    profile = Profile.where(["nim = ?", params[:nim]]).where(["nama = ?", params[:nama]]).where(["approved = ?", true]).first
    generated_password = Devise.friendly_token.first(16)
    if size == 1
      user = User.find(profile.user_id)
      user.password = generated_password
      user.password_confirmation = generated_password
      user.save
      UserMailer.verification(params[:email], user, generated_password).deliver
      redirect_to user_verifikasi_alumni_path, :notice => "NIA dan Password telah dikirim ke email anda"
    elsif size == 0
      redirect_to user_verifikasi_alumni_path, :notice => "Maaf data yang anda maksud belum tersedia"
    end
  end
end