class Admin::DashboardsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @size = User.includes(:roles).where("roles.name = 'user'").where(:approved => true)
    @users = User.includes(:roles).where("roles.name = 'user'").where(:approved => true)
    filename =  "DataAlumni.xls"
    respond_to do |format|
      format.html {}
#      format.csv { send_data @users.to_csv }
#      format.xls # { send_data @forms.to_csv(col_sep: "\t") }
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end
  end
end
