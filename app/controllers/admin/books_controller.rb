class Admin::BooksController < ApplicationController
  before_filter :authenticate_user!
  def index
    @profiles = Profile.where(:approved => true).order('tahun_wisuda ASC')
  end
  
  def show
    @profile = Profile.find_by_tahun_wisuda(params[:id])
  end
  
  def download
    @profile = Profile.find_by_tahun_wisuda(params[:id])
    filename =  "Data-Wisuda-Tahun-#{params[:id]}.xls"
    
    respond_to do |format|
      format.html {}
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end
  end
end
