class Admin::UsersController < ApplicationController
  before_filter :authenticate_user!

  def index
    terakhir = "%04d" % (User.includes(:roles).where("roles.name = 'user'").where(:approved => true).size).to_s
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.includes(:roles).where("roles.name = 'user'")
    @usersfalse = User.includes(:roles).where("roles.name = 'user'").where(:approved => false)
    @userstrue = User.includes(:roles).where("roles.name = 'user'").where(:approved => true)
    @userslast = User.includes(:roles).where("roles.name = 'user'").where(:approved => true).limit(1).where("users.nomor_urut = ?", "#{terakhir}")
  end
  
  def new
    authorize! :new, @user, :message => 'Not authorized as an administrator.'
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  end
  
  def edit
    authorize! :new, @user, :message => 'Not authorized as an administrator.'
    @profile = Profile.find(params[:id])
  end
  
  def update
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(params[:id])
    @user.nomor_urut = (@user.profile.nia).split(".")[3]
    if @user.update_attributes(params[:user], :as => :admin)
      @user.profile.email = @user.email
      @user.profile.nia = "#{@user.profile.concentration.value}.#{(@user.profile.tahun_masuk).strftime("%y")}.#{(@user.profile.tgl_lulus).to_date.strftime("%m%y")}.#{@user.nomor_urut}"
      @user.profile.save
      redirect_to admin_users_path, :notice => "User updated."
    else
      redirect_to admin_users_path, :alert => "Unable to update user."
    end
  end
  
  def create
    @user = User.new(params[:user])
    if @user.save
      flash[:notice] = "Successfully created User." 
      redirect_to admin_users_path
    else
      redirect_to admin_users_path
    end
  end
    
  def destroy
    authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to admin_users_path, :notice => "User deleted."
    else
      redirect_to admin_users_path, :notice => "Can't delete yourself."
    end
  end
  
  def reset_password
    user = User.find(params[:id])
    generated_password = '123456'
    user.password = generated_password
    user.password_confirmation = generated_password
    if user.save
      UserMailer.reset_password(user, generated_password).deliver
      redirect_to admin_users_path, :notice => "Password Reset Successful."
    end
  end
  
  def aproval
    user = User.find(params[:id])
    if user.approved == false
      user.approved = true
      user.username = "#{user.profile.concentration.value}.#{(user.profile.tahun_masuk).strftime("%y")}.#{(user.profile.tgl_lulus).to_date.strftime("%m%y")}.""%04d" % (User.includes(:roles).where("roles.name = 'user'").where(:approved => true).size + 1).to_s
      user.nomor_urut = (user.username).split(".")[3]
      user.profile.nia = user.username
      user.profile.tahun_wisuda = ((user.profile.tgl_lulus).to_date).strftime("%Y")
      user.profile.approved = true
      user.save
      UserMailer.approval(user).deliver
    end        
    #@user.update_attributes(:password => '12345678', :password_confirmation => '123456789')
    redirect_to admin_users_path, :notice => "Aproval Successful."
  end
  
  def unaproval
    user = User.find(params[:id])
    if user.approved == true
      user.approved = false
      user.username = user.profile.nim
      user.nomor_urut = ""
      user.profile.nia = user.username
      user.profile.tahun_wisuda = ((user.profile.tgl_lulus).to_date).strftime("%Y")
      user.profile.approved = false
      user.save
      UserMailer.approval(user).deliver
    end        
    #@user.update_attributes(:password => '12345678', :password_confirmation => '123456789')
    redirect_to admin_users_path, :notice => "Unaproval Successful."
  end
  
  def all_approval
    user = User.find_all_by_approved(false)
    user.each do |user_profile|
      if user_profile.approved == false
        user_profile.approved = true
        user_profile.username = "#{user.profile.concentration.value}.#{(user.profile.tahun_masuk).strftime("%y")}.#{(user.profile.tgl_lulus).to_date.strftime("%m%y")}.""%04d" % (User.includes(:roles).where("roles.name = 'user'").where(:approved => true).size + 1).to_s
        user_profile.nomor_urut = (user.username).split(".")[3]
        user_profile.profile.nia = user_profile.username
        user_profile.profile.approved = true
        user_profile.profile.tahun_wisuda = ((user_profile.profile.tgl_lulus).to_date).strftime("%Y")
        user_profile.save
        UserMailer.approval(user_profile).deliver
      end
    end
    redirect_to admin_users_path, :notice => "All Aproval Successful."
  end
end
