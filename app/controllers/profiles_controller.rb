class ProfilesController < ApplicationController
  before_filter :authenticate_user!
  # GET /profiles
  # GET /profiles.json
  def index
    @profiles = Profile.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @profiles }
    end
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
    if  current_user.profile.blank? == true 
      redirect_to new_profiles_url
    else
      @profile = !params[:id].nil? ? Profile.find(params[:id]) : current_user.profile
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @profile }
    end
  end

  # GET /profiles/new
  # GET /profiles/new.json
  def new
    @profile = Profile.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @profile }
    end
  end

  # GET /profiles/1/edit
  def edit
    #@profile = Profile.find(params[:id])
    @profile = !params[:id].nil? ? Profile.find(params[:id]) : current_user.profile
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(params[:profile])
    respond_to do |format|
      if @profile.save
        @profile.user.email = @profile.email
        @profile.user.save
        format.html { redirect_to root_url, notice: 'Profile was successfully created.' }
        format.json { render json: @profile, status: :created, location: @profile }
      else
        format.html { render action: "new" }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /profiles/1
  # PUT /profiles/1.json
  def update
    @profile = Profile.find(params[:id])
    @profile.user.nomor_urut = (@profile.user.username).split(".")[3]
    @profile.user.save
    respond_to do |format|
      if @profile.update_attributes(params[:profile])        
        if current_user.roles.first.name == "user" 
          @profile.user.email = @profile.email
          @profile.user.save
          @profile.save
          format.html { redirect_to root_url, notice: 'Profile was successfully updated.' }
        elsif current_user.roles.first.name == "admin"
          if @profile.user.approved == true
             @profile.nia = "#{@profile.concentration.value}.#{(@profile.tahun_masuk).strftime("%y")}.#{(@profile.tgl_lulus).to_date.strftime("%m%y")}.#{@profile.user.nomor_urut}"
            @profile.user.username = "#{@profile.concentration.value}.#{(@profile.tahun_masuk).strftime("%y")}.#{(@profile.tgl_lulus).to_date.strftime("%m%y")}.#{@profile.user.nomor_urut}"
          end
          @profile.user.email = @profile.email
          @profile.save
          @profile.user.save
          format.html { redirect_to admin_users_url, notice: 'Profile was successfully updated.' }
        end
        format.json { head :no_content }
      else
        if current_user.roles.first.name == "user"
          format.html { redirect_to root_url, notice: 'Profile was not successfully updated.' }
        elsif current_user.roles.first.name == "admin"
          format.html { redirect_to edit_admin_user_path(@profile.id), notice: 'Profile was not successfully updated.' }
        end 
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile = Profile.find(params[:id])
    @profile.destroy

    respond_to do |format|
      format.html { redirect_to profiles_url }
      format.json { head :no_content }
    end
  end
end
