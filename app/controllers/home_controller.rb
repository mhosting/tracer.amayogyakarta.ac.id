class HomeController < ApplicationController
  def index
    @users = User.includes(:roles).where("roles.name = 'user'")
  end
end
