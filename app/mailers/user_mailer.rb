class UserMailer < ActionMailer::Base
  default from: "TRACER STUDY<tracer@amayogyakarta.ac.id>"
  
  def approval(mail)
    @user = mail
    mail(to: @user.email, subject: 'Akun Anda Telah Aktif')
  end
  
  def reset_password(mail, generated_password)
    @generated_password = generated_password
    @user = mail
    mail(to: @user.email, subject: 'Reset Password')
  end
  
  def verification(email, user, generated_password)
    @generated_password = generated_password
    @user = user
    mail(to: email, subject: 'Verifikasi Data Alumni')
  end
end
