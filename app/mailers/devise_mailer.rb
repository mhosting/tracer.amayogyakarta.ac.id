class DeviseMailer < Devise::Mailer
  default from: "TRACER STUDY <from@example.com>"
  
  def confirmation_instructions(record, token, opts={})
    super
  end
  
  def reset_password_instructions(record, token, opts={})
    @token = token
    devise_mail(record, :reset_password_instructions, opts)
  end
  
  def approval(mail)
    @user = mail
    mail(to: @user.email, subject: 'Akun Anda Telah Aktif')
  end
  
  def reset_password(mail)
    
  end
  
end
