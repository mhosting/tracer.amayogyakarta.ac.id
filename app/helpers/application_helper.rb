module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  

  def avatar_url(user, size)
      gravatar_id = Digest::MD5.hexdigest(user.downcase)
      "http://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}"
  end
  
  def indonesia_date(date)
    if date.strftime("%m") == "01"
      "#{date.strftime("%d")} Januari #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "02"
      "#{date.strftime("%d")} Februari #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "03"
      "#{date.strftime("%d")} Maret #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "04"
      "#{date.strftime("%d")} April #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "05"
      "#{date.strftime("%d")} Mei #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "06"
      "#{date.strftime("%d")} Juni #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "07"
      "#{date.strftime("%d")} Juli #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "08"
      "#{date.strftime("%d")} Agustus #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "09"
      "#{date.strftime("%d")} September #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "10"
      "#{date.strftime("%d")} Oktober #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "11"
      "#{date.strftime("%d")} November #{date.strftime("%Y")}"
    elsif date.strftime("%m") == "12"
      "#{date.strftime("%d")} Desember #{date.strftime("%Y")}"
    end
  end

end
