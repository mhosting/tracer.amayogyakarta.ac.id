module Admin::BooksHelper
  def space(x)
    x.times do |i|
      '&nbsp;'
    end
  end
end
